//
//  ViewController.swift
//  AgeWizard
//
//  Created by Rafael Munhoz on 02/06/15.
//  Copyright (c) 2015 Rafael Munhoz. All rights reserved.
//

import UIKit
import GoogleMobileAds
import AFNetworking

class ViewController: GAITrackedViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GADInterstitialDelegate {
    
    @IBOutlet weak var AdsView: GADBannerView!
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var filePickerButton: UIButton!
    
    @IBOutlet weak var pictureViewer: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    
    var scanRectangleView:UIView?
    var imageToScan:UIImageView!
    
    var imgPickerCtrl:UIImagePickerController? = nil
    var animation:Animation = Animation()
    
    var interstitial:GADInterstitial?
    
    var internetConnection = false
    
    let interstitialId = "ca-app-pub-7128314857798246/7036348244"
    let bannerId = "ca-app-pub-7128314857798246/4082881849"
    
    func hideViewsBeforShare(){
        AdsView.hidden = true
        cameraButton.hidden = true
        filePickerButton.hidden = true
        shareButton.hidden = true
    }
    
    func showViewAfterShare(){
        AdsView.hidden = false
        cameraButton.hidden = false
        filePickerButton.hidden = false
        shareButton.hidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName = "AgeWizzardMainScreen"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AFNetworkReachabilityManager.sharedManager().setReachabilityStatusChangeBlock { (status) -> Void in
            println("Network changed \(status.rawValue)")
            if status.rawValue == 0 {
                self.internetConnection = false
            }else{
                self.internetConnection = true
            }
        }
        AFNetworkReachabilityManager.sharedManager().startMonitoring()
        
        
        animation.animateButtons(cameraButton, delayFactor: 0.2)
        animation.animateButtons(filePickerButton, delayFactor: 0.5)
        interstitial = self.loadAdsInterstitial()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func ShowImagePickerForCamera() {
        if self.internetConnection {
            var tracker = GAI.sharedInstance().defaultTracker
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("UI", action: "Button", label: "Camera", value: nil).build() as
                [NSObject : AnyObject])
            
            self.loadAdsView()
            self.showImagePickerForSourceType(UIImagePickerControllerSourceType.Camera)
        }else{
            self.presentViewController(ErrorHandler().noInternetConnection(), animated: true, completion: nil)
        }
        
    }
    
    @IBAction func ShowImagePickerForPhotoPicker() {
        
        if self.internetConnection {
            var tracker = GAI.sharedInstance().defaultTracker
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("UI", action: "Button", label: "Gallery", value: nil).build() as
                [NSObject : AnyObject])
            if self.adsChooser() {
                self.showImagePickerForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)
            }
        }else{
            self.presentViewController(ErrorHandler().noInternetConnection(), animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func done(sender: UIBarButtonItem) {
        println("Done")
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    @IBAction func takePhoto(sender: UIBarButtonItem) {
        println("takePhoto")
        imgPickerCtrl?.takePicture()
    }
    
    @IBAction func shareAction(sender: UIButton) {
        if internetConnection {
            println("share !")
            var tracker = GAI.sharedInstance().defaultTracker
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("UI", action: "Button", label: "Share", value: nil).build() as [NSObject : AnyObject])
            let textToShare = "Text to share !"
            let imageToShare = self.getImageFromScreen()
            let items:[AnyObject] = [imageToShare]
            let activityViewControler = UIActivityViewController(activityItems: items, applicationActivities: nil)
            activityViewControler.excludedActivityTypes = [UIActivityTypeAssignToContact, UIActivityTypePrint]
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad {
                if (activityViewControler.popoverPresentationController?.respondsToSelector(Selector("popoverPresentationController")) != nil){
                    activityViewControler.popoverPresentationController!.sourceView = view
                }
            }
            
            self.presentViewController(activityViewControler, animated: true, completion: nil)
        }else{
            self.presentViewController(ErrorHandler().noInternetConnection(), animated: true, completion: nil)
        }
        
    }
    
    func adsChooser() -> Bool{
        let random = arc4random_uniform(100)
        if random > 5 {
            loadAdsView()
            return true // no interstitial
        }else{
            if (self.interstitial?.isReady != nil) {
                self.interstitial?.presentFromRootViewController(self)
                self.interstitial = self.loadAdsInterstitial()
            }
            return false // interstitial
        }
    }
    
    func loadAdsInterstitial() -> GADInterstitial{
        
        let interstitialAds = GADInterstitial(adUnitID: self.interstitialId)
        interstitialAds.delegate = self
        let request = GADRequest()
        //request.testDevices = ["fbbdc1e9d092a128e1f7eddc579e10a3",kGADSimulatorID]
        interstitialAds.loadRequest(request)
        
        return interstitialAds
    }
    
    func loadAdsView(){
        self.AdsView.adUnitID = self.bannerId
        self.AdsView.adSize = kGADAdSizeBanner
        self.AdsView.rootViewController = self
        let request = GADRequest()
        //request.testDevices = ["fbbdc1e9d092a128e1f7eddc579e10a3",kGADSimulatorID]
        self.AdsView.loadRequest(request)
    }
    
    func showImagePickerForSourceType(sourceType:UIImagePickerControllerSourceType){
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePickerController.sourceType = sourceType
        imagePickerController.delegate = self
        self.imgPickerCtrl = imagePickerController
        self.presentViewController(imagePickerController, animated: true) { () -> Void in
            
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        println("imagePickerController")
        
        if (self.interstitial?.isReady != nil) {
            self.interstitial?.presentFromRootViewController(self)
            self.interstitial = self.loadAdsInterstitial()
        }
        
        self.shareButton.hidden = true
        self.animation.stopScanAnimation = false
        
        let testImageView = UIImageView(frame: self.pictureViewer.frame)
        
        self.imageToScan = testImageView
        
        for subview in self.pictureViewer.subviews {
            subview.removeFromSuperview()
        }
        
        println("Scaled image height \(pictureViewer.frame.size.height)")
        println("Scaled image width \(pictureViewer.frame.size.width)")
        
        
        testImageView.frame = CGRect(x: 0, y: 0, width: self.pictureViewer.frame.size.width, height: self.pictureViewer.frame.size.height)
        testImageView.contentMode = UIViewContentMode.ScaleAspectFit
        testImageView.image = image
        
        
        testImageView.layer.shadowOffset = CGSize(width: 3, height: 2)
        testImageView.layer.shadowRadius = 3
        testImageView.layer.shadowColor = UIColor.blackColor().CGColor
        testImageView.layer.shadowOpacity = 0.75
        
        
        testImageView.layer.borderColor = UIColor.blackColor().CGColor
        testImageView.layer.masksToBounds = false
        
        
        let facesService = FacesService()
        
        println("Image Description \(image.description)")
        
        facesService.fetchPhotoInformation(UIImageJPEGRepresentation(image, 0.0), callBack: { (facesList:[FacesService.FaceInformation]) -> () in
            println("Number of faces) \(facesList.count)")
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                var tracker = GAI.sharedInstance().defaultTracker
                
                
                if facesList.count > 0 {
                    
                    tracker.send(GAIDictionaryBuilder.createEventWithCategory("Global", action: "Face Detection", label: "Number of Faces", value: facesList.count).build() as
                        [NSObject : AnyObject])
                    
                    self.drawAgeBox(testImageView,facesList:facesList)
                    self.addWaterMark()
                }else{
                    
                    tracker.send(GAIDictionaryBuilder.createEventWithCategory("Global", action: "Face Detection", label: "Error", value: facesList.count).build() as
                        [NSObject : AnyObject])
                    
                    
                    self.presentViewController(ErrorHandler().noFacesFound(), animated: true, completion: nil)
                }
                
                self.animation.doneWithScan(self.scanRectangleView!)
                
            })
        })
        
        self.pictureViewer.image = nil
        self.pictureViewer.addSubview(testImageView)

        scanRectangleView?.removeFromSuperview()
        scanRectangleView = self.drawRectScanner()
        
        self.view.addSubview(self.scanRectangleView!)
       
        let scaleFactor = imageScaleSize(self.imageToScan!)
        
        let scanImageOriginalHeight = self.imageToScan.image?.size.height
        let scanImageOriginalWidth = self.imageToScan.image?.size.width
        
        let scaledHeight = scaleFactor * scanImageOriginalHeight!
        let scaledWidth = scaleFactor * scanImageOriginalWidth!

        
        animation.animateScanRight(self.scanRectangleView!, maxWidth: self.pictureViewer.center.x + scaledWidth/2, minWidth: self.scanRectangleView!.frame.origin.x, amimationDelay:0.3)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func drawRectScanner() -> UIView {
        
        //I was almost drunk when I did this code and it worked :)
        
        let scaleFactor = imageScaleSize(self.imageToScan!)
        let scanImageOriginalHeight = self.imageToScan.image?.size.height
        let scanImageOriginalWidth = self.imageToScan.image?.size.width
        
        let scaledHeight = scaleFactor * scanImageOriginalHeight!
        let scaledWidth = scaleFactor * scanImageOriginalWidth!
        
        let scanlineSize = CGSize(width: 5, height: scaledHeight)
        
        
        let scanRectOrigin = CGPoint(x: self.pictureViewer.center.x - scaledWidth/2, y: self.pictureViewer.center.y - scaledHeight/2)
        
        let scanRectangleView = UIView(frame: CGRect(origin: scanRectOrigin, size: scanlineSize))
        
        scanRectangleView.backgroundColor = UIColor.greenColor()
        scanRectangleView.layer.shadowOffset = CGSize(width: 2, height: 1)
        scanRectangleView.layer.shadowRadius = 3
        scanRectangleView.layer.shadowColor = UIColor.greenColor().CGColor
        scanRectangleView.layer.shadowOpacity = 1
        scanRectangleView.alpha = 0.60
        scanRectangleView.layer.masksToBounds = false
        
        return scanRectangleView
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        println("imagePickerControllerDidCancel")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func drawAgeBox(picture:UIImageView, facesList:[FacesService.FaceInformation]){
        
        let imageSizeFactor = imageScaleSize(picture)
        let posFix = positionFix(picture, scaleFactor: imageSizeFactor)
        var delayFactor:Double = 0.5
        
        for face in facesList {
            
            let faceFrame = CGRect(x:posFix.xOffset + (face.faceRectangle.origin.x * imageSizeFactor), y:posFix.yOffset + (face.faceRectangle.origin.y * imageSizeFactor), width: face.faceRectangle.width * imageSizeFactor, height:face.faceRectangle.height * imageSizeFactor)
            
            let faceView = UIView(frame: faceFrame)
            faceView.backgroundColor = UIColor.magentaColor()
            faceView.alpha = 0
            faceView.hidden = true
            self.pictureViewer.subviews.first?.addSubview(faceView)
            
            var genderImage = ""
            
            if face.gender == "female"{
                genderImage = "age_balloon_female.9"
            }else{
                genderImage = "age_balloon_male.9"
            }
            
            let ageImage = UIImage(named: genderImage)
            
            
            let ageImageView = UIImageView(image: ageImage)
            ageImageView.alpha = 0.75
            
            let textFrame = CGRect(x: ageImageView.frame.origin.x, y: ageImageView.frame.origin.y, width: ageImageView.frame.size.width - 10, height: ageImageView.frame.size.height)
            let ageLabel = UILabel(frame: textFrame)
            ageLabel.text = "\(face.age)"
            ageLabel.textAlignment = NSTextAlignment.Right
            
            
            ageImageView.addSubview(ageLabel)
            
            ageImageView.frame.origin = CGPoint(x: faceFrame.origin.x - (ageImageView.frame.size.width/2), y: faceFrame.origin.y - ageImageView.frame.size.height * 1.3)
            
            ageImageView.transform = CGAffineTransformMakeScale(0.75, 0.75)
            
            self.pictureViewer.subviews.first?.addSubview(ageImageView)
            self.animation.animatePopAgeIcon(ageImageView, delayFactor: delayFactor, totalFaces: facesList.count, shareButton: shareButton)
            
            delayFactor += 0.2
            
            
        }
    }
    
    func positionFix(imageview:UIImageView, scaleFactor:CGFloat) -> (xOffset:CGFloat,yOffset:CGFloat) {
        // check if width is greather than height, I don't now what to do when is equal
        
        var picViewCenterYposition:CGFloat = 0
        var halfScaledImageHeight:CGFloat = 0
        
        var picViewCenterXposition:CGFloat = 0
        var halfScaledImageWidth:CGFloat = 0
        
        var xOffSet:CGFloat = 0
        var yOffSet:CGFloat = 0
        
        if imageview.image?.size.width > imageview.image?.size.height {
            picViewCenterYposition = imageview.center.y
            halfScaledImageHeight = (imageview.image!.size.height * scaleFactor)/2
            yOffSet = picViewCenterYposition - halfScaledImageHeight
        }else{
            picViewCenterXposition = imageview.center.x
            halfScaledImageWidth = (imageview.image!.size.width * scaleFactor)/2
            xOffSet =  picViewCenterXposition - halfScaledImageWidth
        }
        
        return (xOffSet,yOffSet)
    }
    
    func imageScaleSize(imageview:UIImageView) -> CGFloat {
        
        let sx = CGFloat(imageview.frame.size.width / imageview.image!.size.width)
        let sy = CGFloat(imageview.frame.size.height / imageview.image!.size.height)
        return fmin(sx, sy)
    }
    
    func getImageHighFromScreen() -> UIImage {
        
        self.hideViewsBeforShare()
        
        
        
        
        
        
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size,false,0.0)
        let context = UIGraphicsGetCurrentContext()
        self.view.layer.renderInContext(context)
        

        
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let rectToCrop = CGRect(x: 0, y: self.view.frame.size.height * 0.10, width: backgroundImage.size.width, height: backgroundImage.size.height * 0.76)
        
        
        let cropedImage = CGImageCreateWithImageInRect(backgroundImage.CGImage, rectToCrop)
        let uiimageCroped = UIImage(CGImage: cropedImage)
        self.showViewAfterShare()
        return uiimageCroped!
        
    }
    
    func getImageFromScreen() -> UIImage {
        
        self.hideViewsBeforShare()
        //UIGraphicsBeginImageContext(self.view.frame.size)
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size,false,0.0)
        let context = UIGraphicsGetCurrentContext()
        self.view.layer.renderInContext(context)
        
        //UIGraphicsBeginImageContextWithOptions()
        
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let rectToCrop = CGRect(x: 0, y: self.view.frame.size.height*2 * 0.10, width: backgroundImage.size.width*2, height: backgroundImage.size.height*2 * 0.76)
        
        
        let cropedImage = CGImageCreateWithImageInRect(backgroundImage.CGImage, rectToCrop)
        let uiimageCroped = UIImage(CGImage: cropedImage)
        self.showViewAfterShare()
        return uiimageCroped!
    }
  
    
    func drawRectAsImageScaner() -> UIView{
        
        let pictureRect = self.pictureViewer.frame
        //let pictureRect = self.imageToScan.frame
        
        let scaleFactor = imageScaleSize(self.imageToScan!)
        let scanlineHeight = self.imageToScan.image?.size.height
        let scaledHeight = scaleFactor * scanlineHeight!
        
        let scanlineSize = CGSize(width: 5, height: scaledHeight)
        
        let posFix = positionFix(self.imageToScan, scaleFactor: scaleFactor)
        //let scanRectOrigin = CGPoint(x: posFix.xOffset + pictureRect.origin.x, y: posFix.yOffset + pictureRect.origin.y)
        let scanRectOrigin = CGPoint(x: posFix.xOffset, y: posFix.yOffset)
        
        let scanRectangleView = UIView(frame: CGRect(origin: scanRectOrigin, size: scanlineSize))
        scanRectangleView.backgroundColor = UIColor.greenColor()
        
        scanRectangleView.layer.shadowOffset = CGSize(width: 2, height: 1)
        scanRectangleView.layer.shadowRadius = 3
        scanRectangleView.layer.shadowColor = UIColor.greenColor().CGColor
        scanRectangleView.layer.shadowOpacity = 1
        scanRectangleView.alpha = 0.60
        scanRectangleView.layer.masksToBounds = false
        
        return scanRectangleView
    }
    
    func addWaterMark(){
        
        let imageSizeFactor = imageScaleSize(self.imageToScan)
        let posFix = positionFix(self.imageToScan, scaleFactor: imageSizeFactor)
        let waterMarkImage = UIImage(named:"text_overlay")
        let waterMarkImageView = UIImageView(image: waterMarkImage)
        let xWaterPosition = self.imageToScan.frame.size.width - waterMarkImageView.frame.size.width - posFix.xOffset
        let yWaterPosition = self.imageToScan.frame.size.height - waterMarkImageView.frame.size.height - posFix.yOffset
        
        waterMarkImageView.frame.origin = CGPointMake(xWaterPosition, yWaterPosition)
        self.imageToScan.addSubview(waterMarkImageView)
        
    }
    
    func interstitialDidDismissScreen(ad: GADInterstitial!) {
        print("Dismissed interstitial !")
        self.showImagePickerForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)
    }
}

