//
//  Animation.swift
//  AgeWizard
//
//  Created by Rafael Munhoz on 15/06/15.
//  Copyright (c) 2015 Rafael Munhoz. All rights reserved.
//

import Foundation
import UIKit

class Animation {
    
    var stopScanAnimation = false
    var maxScanAnimationWidth:CGFloat = 0
    var minScanAnimationWidth:CGFloat = 0
    var popedAgeViews = 0
    
    func animateButtons(view:UIView, delayFactor:Double){
        
        view.transform = CGAffineTransformMakeScale(0.01, 0.01)
        UIView.animateWithDuration(0.1, delay: delayFactor, options: nil, animations: {
            view.transform = CGAffineTransformMakeScale(1, 1)
            },
            completion: nil)
    }
    
    func animatePopAgeIcon(view:UIView, delayFactor:Double, totalFaces:Int, shareButton:UIButton){

        view.transform = CGAffineTransformMakeScale(0.01, 0.01)
        UIView.animateWithDuration(0.2, delay: delayFactor, options: nil, animations: {
            view.transform = CGAffineTransformMakeScale(0.75, 0.75)
            },
            completion: {Bool in
                self.popedAgeViews++
                if self.popedAgeViews == totalFaces{
                    shareButton.hidden = false
                    self.animateButtons(shareButton, delayFactor: 0)
                    self.popedAgeViews = 0
                }
        })
    }
    
    func animateScanRight(scanRectangleView:UIView, maxWidth:CGFloat, minWidth:CGFloat, amimationDelay:NSTimeInterval){
        
        self.maxScanAnimationWidth = maxWidth
        self.minScanAnimationWidth = minWidth
        UIView.animateWithDuration(1, delay:amimationDelay, options:UIViewAnimationOptions.CurveEaseInOut, animations:
        {
                scanRectangleView.frame.origin.x = self.maxScanAnimationWidth
                
            }, completion:{Bool in
                
                if self.stopScanAnimation != true{
                    self.animateScanLeft(scanRectangleView)
                }
            }
        )
    }
    
    func animateScanLeft(scanRectangleView:UIView){
        UIView.animateWithDuration(1, delay: 0,
            options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                scanRectangleView.center.x = self.minScanAnimationWidth
            }, completion: {Bool in
                if self.stopScanAnimation != true{
                    
                    self.animateScanRight(scanRectangleView,maxWidth:self.maxScanAnimationWidth,minWidth:self.minScanAnimationWidth,amimationDelay: 0)
                }
        })
    }
    
    func doneWithScan(scanRectangleView:UIView){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            scanRectangleView.alpha = 0
            self.stopScanAnimation = true
            self.maxScanAnimationWidth = 0
        })
    }
}
