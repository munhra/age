//
//  ErrorHandler.swift
//  AgeWizard
//
//  Created by Rafael Munhoz on 15/06/15.
//  Copyright (c) 2015 Rafael Munhoz. All rights reserved.
//

import Foundation
import UIKit

class ErrorHandler{
    
    var alertMessage:UIAlertController? = nil
    
    
    func noInternetConnection() -> UIAlertController {
        
        alertMessage = UIAlertController(title: "Ops! No Internet Connection", message: "Please check your internet connection and try again", preferredStyle: UIAlertControllerStyle.Alert)
        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("dismissAlert"), userInfo: nil, repeats: false)
        return self.alertMessage!
        
    }
    
    func noFacesFound() -> UIAlertController {
        
        alertMessage = UIAlertController(title: "Ops! No Faces", message: "There is no faces on the picture", preferredStyle: UIAlertControllerStyle.Alert)
        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("dismissAlert"), userInfo: nil, repeats: false)
        return self.alertMessage!
    }
    
    @objc func dismissAlert() {
        self.alertMessage!.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

