//
//  FacesService.swift
//  AgeWizard
//
//  Created by Rafael Munhoz on 03/06/15.
//  Copyright (c) 2015 Rafael Munhoz. All rights reserved.
//

import Foundation
import UIKit

class FacesService{
    
    /*
    Iphone image sizes (Back Camera)
    3264x2448 iPhone 6 plus, 6, 5, 4s 8 megapixel
    2592x1936 iPhone 4, iPad 3, iPod Touch
    
    Supported library images sizes
    
    */
    
    struct FaceInformation{
        
        var faceRectangle:CGRect
        var age:Int
        var gender:String
        
    }
    
    func convertJsonToFaceObject(responseData:NSData) -> [FaceInformation]{

        var faceInformationList = [FaceInformation]()

        if let jsonObject:NSArray = NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSArray{
    
            for item in jsonObject{
                
                let faceDict = item as? NSDictionary
                let attributesDict = faceDict?.objectForKey("attributes") as? NSDictionary
                let age = attributesDict?.objectForKey("age") as? Int
                let faceGender = attributesDict?.objectForKey("gender") as? String
                let faceRectangleDict = faceDict?.objectForKey("faceRectangle") as? NSDictionary
                let faceHeight = faceRectangleDict?.objectForKey("height") as? Int
                let faceLeft = faceRectangleDict?.objectForKey("left") as? Int
                let faceTop = faceRectangleDict?.objectForKey("top") as? Int
                let faceWidth = faceRectangleDict?.objectForKey("width") as? Int
                let faceRect = CGRect(x: faceLeft!, y: faceTop!, width: faceWidth!, height: faceHeight!)
                
                let faceInformation = FaceInformation(faceRectangle: faceRect, age: age!, gender: faceGender!)
                
                faceInformationList.append(faceInformation)
            }
        }
    
        return faceInformationList
    }
    
    
    func fetchPhotoInformation(imageData:NSData, callBack:([FaceInformation]) -> ()){
        
        let oxfordUrl = NSURL(string: "https://api.projectoxford.ai/face/v0/detections?analyzesAge=true&analyzesGender=true")
        let session = NSURLSession.sharedSession()
        let oxfordRequest:NSMutableURLRequest = NSMutableURLRequest(URL:oxfordUrl!)
        oxfordRequest.HTTPMethod = "POST"
        oxfordRequest.addValue("8243043be1d548ce8d4c362cf5347f21", forHTTPHeaderField: "Ocp-Apim-Subscription-Key")
        oxfordRequest.addValue("api.projectoxford.ai", forHTTPHeaderField: "Host")
        oxfordRequest.addValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
        
        var stringData = NSString(string: "{\"url\":\"https://farm3.static.flickr.com/2064/1566247441_4f49f3c7a4.jpg\"}")
        var bodyData = stringData.dataUsingEncoding(NSUTF8StringEncoding)
        oxfordRequest.HTTPBody = imageData
        
        println("Request description \(oxfordRequest.description)")
        
        let dataTask = session.dataTaskWithRequest(oxfordRequest, completionHandler: { (data: NSData!, response:NSURLResponse!,
            error: NSError!) -> Void in
            var datastr = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("data \(datastr!)")
            callBack(self.convertJsonToFaceObject(data!))
        })
        
        dataTask.resume()
    }

}